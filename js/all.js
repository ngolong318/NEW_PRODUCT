(function() {
    var carousels = document.querySelectorAll('.carousel');
    
    [].forEach.call(carousels, function(carousel) {
      carouselize(carousel);
    });
    
  })();
  
  function carouselize(carousel) {
    var productList = carousel.querySelector('.product-list');
    var productListWidth = 0;
    var productListSteps = 0;
    var products = carousel.querySelectorAll('.product');
    var productAmount = 0;
    var productAmountVisible = 4;
    var carouselPrev = carousel.querySelector('.carousel-prev');
    var carouselNext = carousel.querySelector('.carousel-next');
  
    [].forEach.call(products, function(product) {
      productAmount++;
      productListWidth += 285;
      productList.style.width = productListWidth+"px";
    });
  
    carouselNext.onclick = function() {
      if(productListSteps < productAmount-productAmountVisible) {
        productListSteps++;
        moveProductList();
      }
    }
    carouselPrev.onclick = function() {
      if(productListSteps > 0) {
        productListSteps--;
        moveProductList();
      }
    }
    
    function moveProductList() {
      productList.style.transform = "translateX(-"+285*productListSteps+"px)";
    }
  }


  if ($('.back-to-top').length) {
    var scrollTrigger = 100, 
        backToTop = function () {
            var scrollTop = $(window).scrollTop();
            if (scrollTop > scrollTrigger) {
                $('.back-to-top').addClass('show');
            } else {
                $('.back-to-top').removeClass('show');
            }
        };
    backToTop();
    $(window).on('scroll', function () {
        backToTop();
    });
    $('.back-to-top').on('click', function (e) {
        e.preventDefault();
        $('html,body').stop().animate({scrollTop:0}, 700);
    });
  }